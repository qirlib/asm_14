global _main

extern _printf
extern _scanf

section .data
    enter_resistances_message: db "Enter resistances of R1, R2, R3, R4:", 10, 0
    scanf_format: db "%lf %lf %lf %lf", 0
    result_message: db "R_c = %.3lf", 10, 0
    invalid_input_message: db "Invalid input", 10, 0

    R1: dq 0
    R2: dq 0
    R3: dq 0
    R4: dq 0
section .text
_main:
    ; R_c = R1 + R2 + R3 + R4

    push enter_resistances_message
    call _printf
    add esp, 4 * 1

    push R1
    push R2
    push R3
    push R4
    push scanf_format
    call _scanf
    add esp, 4 * 5

    cmp eax, 4
    jne exit_with_error

    fldz
    fld qword [R1]
    fcomi ST1
    jbe pop_fpu_2_exit_with_error
    fld qword [R2]
    fcomi ST2
    jbe pop_fpu_3_exit_with_error
    fld qword [R3]
    fcomi ST3
    jbe pop_fpu_4_exit_with_error
    fld qword [R4]
    fcomi ST4
    jbe pop_fpu_5_exit_with_error
    
    faddp
    faddp
    faddp
    faddp
    fstp qword [R1]
    
    push dword [R1 + 4]
    push dword [R1]
    push result_message
    call _printf
    add esp, 4 * 1 + 8

    xor eax, eax
    ret
pop_fpu_5_exit_with_error: ; убираем нужное кол-во элементов со стека fpu
    fstp ST0
pop_fpu_4_exit_with_error:
    fstp ST0
pop_fpu_3_exit_with_error:
    fstp ST0
pop_fpu_2_exit_with_error:
    fstp ST0
    fstp ST0
exit_with_error:
    push invalid_input_message
    call _printf

    mov eax, 1
    ret

global _main

extern _printf
extern _scanf

section .data
     a: dq 0
     enter_input_message: db "Enter a:", 10, 0
     scanf_format: db "%lf", 0
     result_message: db "R = %.3lf", 10, 0
     invalid_input_message: db "Invalid input", 10, 0

section .text
_main:
    ; R = a / sqrt(3)

    push enter_input_message
    call _printf
    add esp, 4 * 1

    push a
    push scanf_format
    call _scanf
    add esp, 4 * 2

    cmp eax, 1 ; _scanf возвращает кол-во успешно заполненных аргументов
    jne exit_with_error

    fld qword [a] ; загрузить [a] на стек fpu
    fldz ; загрузить 0 на стек

    fcomip ST1 ; сравнить с нулём на стеке, убрать со стека
    jae pop_stack_exit_with_error

    fld1  ; Получить sqrt(3) чисто инструкциями
    fld1
    fadd ST1, ST0 ; ST1 += ST0
    faddp
    fsqrt

    fdivp ST1, ST0 ; ST1 = ST1 / ST0, убрать вершину стека

    fstp qword [a]

    push dword [a + 4] ; push qword [...] в 32-битном режиме не поддерживается
    push dword [a]
    push result_message
    call _printf
    add esp, 4 * 1 + 8

    xor eax, eax
    ret
pop_stack_exit_with_error:
    fstp ST0 ; сбрасываем ненужное значение со стека x87 fpu
exit_with_error:
    push invalid_input_message
    call _printf
    add esp, 4 * 1

    mov eax, 1 ; код ошибки, возвращаемый процессом
    ret

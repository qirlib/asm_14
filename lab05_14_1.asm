global _main

extern _printf
extern _scanf

section .data
    enter_data_message: db "Enter a, b:", 10, 0
    scanf_format: db "%lf %lf", 0
    result_message: db "X = %lf", 10, 0
    invalid_input_message: db "Invalid input", 10, 0
    div_by_zero_message: db "Division by zero", 10, 0

    a: dq 0
    b: dq 0

section .text
_main:
    push enter_data_message
    call _printf
    add esp, 4 * 1

    push b
    push a
    push scanf_format
    call _scanf
    add esp, 4 * 3

    cmp eax, 2
    jne exit_with_error

    fld qword [a]
    fld qword [b]
    fcomi ST1
    je .a_eq_b
    jb .a_above_b
.a_below_b:
    fldz
    fcomip ST1
    je exit_with_div_by_zero

    fld ST0 ; дублируем b
    fmul ST0, ST2
    fxch ST2
    fdivrp ; ST1 = ST0 / ST1
    fld1
    fsubp
    faddp
    fstp qword [a]
    jmp .display_result
.a_eq_b: ; при условии a == b формула сокращается до f(a, b) = 1 и достаточно
         ; записать fstp ST0; fstp ST0; fld1; fstp qword [a]
         ; Тем не менее, сделано точно/тупо по формуле
    fld ST0
    fmul ST0, ST0
    fmul ST0, ST1
    fadd ST0, ST2
    fld1 ; четыре программными средствами
    fld1
    fadd ST0, ST1
    fadd ST0, ST1
    faddp
    faddp ; посчитали вторую скобку, в ST0
    fxch ST2
    fsubp
    fmulp
    fld1
    faddp
    fstp qword [a]
    jmp .display_result
.a_above_b:
    fmul ST0, ST0
    fsubp
    fmul ST0, ST0
    fstp qword [a]
.display_result:
    push dword [a + 4]
    push dword [a]
    push result_message
    call _printf
    add esp, 4 * 1 + 8

    xor eax, eax
    ret
exit_with_error:
    push invalid_input_message
    call _printf
    add esp, 4 * 1
    
    mov eax, 1
    ret
exit_with_div_by_zero:
    fstp ST0 ; очищаем стек от a, b
    fstp ST0
    
    push div_by_zero_message
    call _printf
    add esp, 4 * 1
    
    mov eax, 2
    ret

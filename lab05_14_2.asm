global _main

extern _printf
extern _scanf

section .data
    enter_input_message: db "Enter N:", 10, 0
    scanf_format: db "%u", 0
    result_message: db "Result: %u", 10, 0
    invalid_input_message: db "Invalid input", 10, 0

    N: dd 0
section .text
_main:
    push enter_input_message
    call _printf
    add esp, 4 * 1

    push N
    push scanf_format
    call _scanf
    add esp, 4 * 2

    cmp eax, 1
    jne exit_with_error

    mov esi, [N]

    cmp esi, 10
    jb skip_to_end ; однозначное число - специальный случай; для него следующий
                   ; алгоритм не подходит

    mov eax, esi
    mov ebx, 10

    xor edx, edx
    div ebx

    mov ecx, edx ; сохраняем последнюю цифру (справа в записи)
    sub esi, edx

    mov edi, 1
.div_loop:
    xchg eax, edi
    mul ebx
    xchg eax, edi

    xor edx, edx
    div ebx

    test eax, eax
    jnz .div_loop

    add esi, edx ; в edx - первая цифра

    mov eax, edi
    mul edx
    sub esi, eax

    mov eax, edi
    mul ecx
    add esi, eax
skip_to_end:
    push esi
    push result_message
    call _printf
    add esp, 4 * 2

    xor eax, eax
    ret
exit_with_error:
    push invalid_input_message
    call _printf
    add esp, 4 * 1
    
    mov eax, 1
    ret
